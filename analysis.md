## Analysis

### Man output
    mkdir [OPTION]... DIRECTORY...

       Create the DIRECTORY(ies), if they do not already exist.

       Mandatory arguments to long options are mandatory for short
       options too.

       -m, --mode=MODE
              set file mode (as in chmod), not a=rwx - umask

       -p, --parents
              no error if existing, make parent directories as needed

       -v, --verbose
              print a message for each created directory

       -Z     set SELinux security context of each created directory to
              the default type

       --context[=CTX]
              like -Z, or if CTX is specified then set the SELinux or
              SMACK security context to CTX

       --help display this help and exit

       --version
              output version information and exit


### Functionality of mkdir

With no options it creates directory with given name. 
If more arguments are provided more directories are made. It can take as many arguments as needed.
Directory is created by default with **a=rwx - umask** permissions. 

#### -m, --mode=MODE
It creates directory with given permissions works as chmod.

#### -p, --parents
Doesn't exit with error when given directory name already exists, and also
it makes parent directories.

####  -v, --verbose
Prints a message for each created directory

#### -Z and --context[=CTX]
Used for setting SELinux security context.(better security rules)

#### --help 
Prints to stdout help message.

#### --version
Outputs version information.


## Testing strategy

Test expected behaviour especially in edge cases. 

### Tests should focus on 
  * Permissions of created directories
  * Exit codes
  * Special characters in directory names
  * Correct path checking
  * Correct prints when verbose option is active
  * Options combinations

### Possible edge cases
  * Special characters in directory names
  * No arguments provided
  * Not enough free space to create new directory (Though if you don't have enough space for dir, you have bigger problems)
  * User not having permissions to create directory
  * File or directory with given name already exists
  * Invalid directory path/name (default is ./<name>)

### Other possible problems
  * Memory leaks
  * Malicious behaviour
  * Program not responding / not ending

### Allowed linux directory names
  * https://www.cyberciti.biz/faq/linuxunix-rules-for-naming-file-and-directory-names/
  * 255 chars on new systems 14 on old
  * every character is allowed except "/"
    

## Automation strategy

Create python script, which calls mkdir program and tests its behaviour.

Use python testing frameworks such as unittest or pytest.

### Possible problems
  * Framework doesn't support required output format ([PASS] \<description of check> ...)
    * Solution https://stackoverflow.com/a/284192
    * https://stackoverflow.com/questions/4414234/getting-pythons-unittest-results-in-a-teardown-method/39606065
    * https://stackoverflow.com/a/63631661
    * https://stackoverflow.com/questions/57888964/how-to-retrieve-the-testresult-object-from-a-python-unittest?noredirect=1&lq=1
    * https://docs.python.org/3/library/unittest.html#unittest.TestResult 
  * SELinux security context testing (not familiar with this enhanced security functionality)
  * Not enough free space testing (not easily simulated)
  * Malicious behaviour testing (not easily detected)
  * Catching cout prints 
    * Solution: Use subprocess library
  
## Test coverage
### Implemented tests cover
 * Behaviour with no options and -p, -m option
   * Creation
   * Permissions
   * Special characters in names
   * stdout and stderr output
   * Exit code
   * Max execution time was set to 1 sec. (wrapper.py timeout)
   * Basic possible side effects (creating more directories, modifying other directories)
 * No arguments and no options behavior

### Implemented tests don't cover
 * Behaviour when you don't have permissions to create directory
 * Options -v, --verbose, -Z,  --help --version
 * Combinations of any options
 * Memory leaks 
 * Advanced malicious behaviour and side effects 
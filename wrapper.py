import logging
import subprocess

timeout = 1


def mkdir(*args: str, options=None):
    """
    Simple wrapper function for system call to mkdir
    :param options:
    :param args:
    :return: return code
    """
    arguments = ""
    for arg in args:
        arguments += " " + arg

    if options:
        finish = subprocess.run(f"mkdir -{options} {arguments}", shell=True, capture_output=True, timeout=timeout,
                                universal_newlines=True)
    else:
        finish = subprocess.run(f"mkdir {arguments}", shell=True, capture_output=True, timeout=timeout,
                                universal_newlines=True)

    logging.debug(f"Running {finish.args}")
    return finish.returncode, finish.stdout, finish.stderr


if __name__ == '__main__':
    pass
    # code, stdout, stderr = mkdir("-la")
    # print(code)
    # print(stdout)
    # print(stderr)

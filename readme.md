# MKDIR tester

## Running the tests
Run **_test_mkdir.py_** on linux machine with **python 3.9**. 
No additional libraries needed, tester uses only core pyton libraries.

## Problem analysis
Problem analysis, testing approach and coverage, possible improvements and more can
be found in **_analysis.md_**
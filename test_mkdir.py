import logging
import os
import unittest

from wrapper import mkdir

logging.basicConfig(encoding='utf-8', level=logging.INFO)

TESTING_DIR = "/tmp/TESTS/"


def get_umask() -> int:
    """
    Gets current umask
    :return:
    """
    umask = os.umask(0o666)
    os.umask(umask)
    return umask


def get_permissions(path) -> int:
    """
    Returns permissions of file/dir with given path
    :param path:
    :return:
    """
    return os.lstat(path).st_mode & 0o777


def get_default_permission() -> int:
    """
    Return default permissions for directory on your system
    :return:
    """
    return 0o777 - get_umask()


class MkDirTestCase(unittest.TestCase):

    def setUp(self) -> None:
        """
        Set up working directory for the tests
        :return:
        """
        self.tearDown()
        os.mkdir(TESTING_DIR)

    def tearDown(self) -> None:
        """
        Remove working directory and what is in it
        :return:
        """
        os.system("rm -rf " + TESTING_DIR)

    def create_and_check(self, dir_name: str, options=None, expected_perm=None):
        """
        Checks creation of directory with given name and options for mkdir
        :param expected_perm: expected permission of created dir
        :param dir_name: name of the dir to be created
        :param options: options for mkdir
        :return:
        """
        # Create dir with escaped name and check return code and prints
        return_code, stdout, stderr = mkdir(TESTING_DIR + "'" + dir_name + "'", options=options)

        self.assertFalse(stdout)
        self.assertFalse(stderr)
        self.assertEqual(0, return_code)

        # Check creation
        directories = os.listdir(TESTING_DIR)
        self.assertIn(dir_name, directories)

        # Check permissions
        if expected_perm is None:
            expected_perm = get_default_permission()

        dir_perms = get_permissions(TESTING_DIR + dir_name)
        self.assertEqual(expected_perm, dir_perms)

        # Check if the created dir is empty
        if dir_perms >= 0o400:
            contents = os.listdir(TESTING_DIR + dir_name)
            self.assertEqual(0, len(contents))

    def test_create_dir_basic(self):
        """
        Basic behaviour test
        :return:
        """
        self.create_and_check("BasicDir")

        # Check if only one dir was created
        directories = os.listdir(TESTING_DIR)
        self.assertEqual(1, len(directories))

    def test_existing_dir(self):
        """
        Existing name test
        :return:
        """
        # Create basic dir
        self.create_and_check("BasicDir")

        # Try to create same dir
        return_code, stdout, stderr = mkdir(TESTING_DIR + "BasicDir", options=None)

        self.assertFalse(stdout)
        self.assertEqual(1, return_code)
        self.assertEqual(stderr, "mkdir: cannot create directory ‘/tmp/TESTS/BasicDir’: File exists\n")

        # Check if the dir wasn't changed
        directories = os.listdir(TESTING_DIR)
        self.assertEqual(1, len(directories))
        self.assertEqual(directories[0], "BasicDir")

        # Check permissions
        dir_perms = get_permissions(TESTING_DIR + "BasicDir")
        self.assertEqual(get_default_permission(), dir_perms)

    def test_ugly_names(self):
        """
        Special characters in name test
        :return:
        """
        ugly_names = ["> < | : & $ ", "14CharLongName", r'|\:()&;?*"\n']

        for name in ugly_names:
            self.create_and_check(name)

        logging.debug(os.listdir(TESTING_DIR))

    def test_no_arg(self):
        """
        No argument test
        :return:
        """
        return_code, stdout, stderr = mkdir()
        self.assertFalse(stdout)
        self.assertEqual(stderr, "mkdir: missing operand\nTry 'mkdir --help' for more information.\n")
        self.assertNotEqual(0, return_code)

        # Check if anything was created
        directories = os.listdir(TESTING_DIR)
        self.assertEqual(0, len(directories))

    def test_multiple_arguments(self):
        """
        Multiple arguments test
        :return:
        """
        # Create 10 dir names
        args = []
        for x in range(0, 10):
            args.append(TESTING_DIR + f"Dir{x}")
        return_code, stdout, stderr = mkdir(*args)

        # Check output
        self.assertEqual(return_code, 0)
        self.assertFalse(stdout)
        self.assertFalse(stderr)

        # Check if everything was created
        directories = os.listdir(TESTING_DIR)
        self.assertEqual(len(args), len(directories))

        # Check correct names
        for x in range(0, 10):
            directory = f"Dir{x}"
            self.assertIn(directory, directories)

        # Check permissions
        for directory in args:
            dir_perms = get_permissions(directory)
            self.assertEqual(get_default_permission(), dir_perms)

    def test_option_m(self):
        """
        Option -m test
        :return:
        """
        # This takes too long
        # for perm in range(0, 0o777):
        #     # https://blog.finxter.com/python-print-octal-without-0o/
        #     perm_string: str = oct(perm)[2:].zfill(3)
        #     self.create_and_check(perm_string, options="m " + perm_string, expected_perm=perm)

        for perm in [0o000, 0o111, 0o222, 0o333, 0o444, 0o555, 0o666, 0o777]:
            # https://blog.finxter.com/python-print-octal-without-0o/
            perm_string: str = oct(perm)[2:].zfill(3)
            self.create_and_check(perm_string, options="m " + perm_string, expected_perm=perm)

    def test_option_p(self):
        """
        Option -p test
        :return:
        """
        # Create dir with escaped name and check return code and prints
        return_code, stdout, stderr = mkdir(TESTING_DIR + "BasicDir/DirInDir", options="p")

        self.assertFalse(stdout)
        self.assertFalse(stderr)
        self.assertEqual(0, return_code)

        # Check creation of root dir
        directories = os.listdir(TESTING_DIR)
        self.assertEqual(1, len(directories))
        self.assertIn("BasicDir", directories)

        # Check creation of nested dir
        directories = os.listdir(TESTING_DIR + "BasicDir")
        self.assertEqual(1, len(directories))
        self.assertIn("DirInDir", directories)

        # Check permissions of root dir
        dir_perms = get_permissions(TESTING_DIR + "BasicDir")
        self.assertEqual(get_default_permission(), dir_perms)

        # Check permissions of nested dir
        dir_perms = get_permissions(TESTING_DIR + "BasicDir/DirInDir")
        self.assertEqual(get_default_permission(), dir_perms)

    def test_option_p_existing(self):
        """
        Option -p test on existing directory
        :return:
        """
        self.create_and_check("BasicDir")

        # Creating dir with same name, should do nothing and exit with 0
        self.create_and_check("BasicDir", options="p")


def get_summary(success: list, fails: list):
    # return f"{len(success)} SUCCESSFUL TESTS {len(fails)} FAILED TEST"
    return f"{len(success) + len(fails)} Checks performed"


if __name__ == '__main__':

    # Collect tests form my testcase
    print("-----------Collecting tests-----------")
    suite = unittest.TestLoader().loadTestsFromTestCase(MkDirTestCase)
    tests = set(suite)
    for test in suite:
        print(test.shortDescription())

    print("\n-----------Running tests-----------")
    # Run tests
    successful_tests = []
    failed_tests = []
    for test in suite:
        result = test.run()  # https://docs.python.org/3/library/unittest.html#unittest.TestResult

        if result.wasSuccessful():
            print(f"[PASS] {test.shortDescription()}")
            successful_tests.append(test)

        else:
            failed_tests.append(test)
            message = ""
            error_string = ""

            # Determine why test failed
            if result.errors:
                # Error / exception thrown
                assert len(result.errors) == 1
                assert len(result.failures) == 0

                message = "error occurred / exception thrown"
                error_string = result.errors[0][1]
            else:
                # Assert field
                assert len(result.errors) == 0
                assert len(result.failures) == 1
                message = "assertion fail"
                error_string = result.failures[0][1]

            print(f"[FAIL] {test.shortDescription()}, {message}")
            print(error_string)

    print("\n-----------Done-----------")

    # Print summary
    if len(failed_tests) == 0:
        print(f"**TEST PASSED: {get_summary(successful_tests, failed_tests)}**")
        exit(0)
    else:
        print(f"**TEST FAILED: {len(failed_tests)}, {get_summary(successful_tests, failed_tests)}**")
        exit(1)
